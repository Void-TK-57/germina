module Model

using Flux

export Network

struct Network
    architecture::Any
    history::Array{Float64,1}
    loss::Function
    opt::Flux.AbstractOptimiser
end

function Network(architecture::Any, loss::Function, opt::Flux.AbstractOptimiser)::Network
    Network(architecture, Array{Float64,1}(), loss, opt)
end

@inline loss(network::Network, X::Matrix, y::Matrix) = network.loss(network.architecture(X), y)
@inline loss(network::Network) = (X,y) -> network.loss(network.architecture(X), y)


function train!(network::Network, data; freeze=false)::Vector{Float64}
    # local loss function to be passed to train!
    local loss = loss(network)

    # layers to be optimzed
    ps = Flux.params(model)
    if freeze != false
        ps = ps[freeze]
    end

    # training loop
    for i in 1:epochs
        # train for 1 epoch
        Flux.train!(loss, ps, [data], network.opt)
        
        # calculate epoch loss, add to history and log to console
        epoch_loss = loss(data...)
        push!(network.history, epoch_loss)
        @info "Epoch $i/$epochs, Loss: $epoch_loss"
    end
end

@inline logistic_regression(input, output) = Flux.Dense(input, output, σ)


function autoencoder(input, layers::Vector)
    previous_output_size = input
    architecture = []

    for (output_size, σ) in layers
        push!(architecture, Dense(previous_output_size, output_size, σ))
        previous_output_size = output_size
    end

    return Flux.Chain(architecture...)
end

function conv_autoencoder()
    return Flux.Chain(
        Flux.Conv((40, 1), 1=>5, stride=2),
        Flux.MeanPool((20, 1), stride=3),
        Flux.Conv((20,1), 5=>15,stride=1),
        Flux.MeanPool((15,1),stride=2),
        Flux.Conv((10,1), 15=>60,stride=1),
        Flux.MeanPool((8,1),stride=4),
        Flux.flatten,
        Flux.Dense(60, 30, tanh),
        Flux.Dense(30, 1, σ)
    )
end

function LeNet5()
    model = Flux.Chain(
        Flux.Conv((5, 5), 1=>6, tanh, stride=1, pad=2),
        Flux.MeanPool((2, 2), stride=2),
        Flux.Conv((5, 5), 6=>16, tanh, stride=1),
        Flux.MeanPool((2, 2), stride=2),
        Flux.Conv((5, 5), 16=>120, tanh, stride=1),
        Flux.flatten,
        Flux.Dense(120, 84, tanh),
        Flux.Dense(84, 10, σ)
    )
    return model
end

end